﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed;
    
    private void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward, speed * Time.deltaTime);
        }
        
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.back, speed * Time.deltaTime);
        }
    }
}

﻿using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    [Header("Output")]
    public MeshFilter targetMeshFilter;
    public MeshRenderer targetMeshRenderer;

    [Header("Settings")]
    public int radius = 4;
    [Min(0)] public int depth = 20;
    [Min(3)] public int edges = 20;
    [Range(0, 360)] public float maxAngle = 45;
    [Min(0)] public float centerRadius = 120;
    
    [Header("Animation")]
    public float speed;
    public float rotationSpeed;
    public float colorizingSpeed;
    public Gradient gradient;
    
    [Header("Tiling")]
    [Min(1)] public Vector2 tiling =  Vector2.one;
    [Space(10)]
    public float autoTilingModifier = 2;
    public bool autoTiling = true;

    [Header("Brightness")]
    public Vector2 blend;
    private Vector2[] _uvs;
    private Vector3[] _vertices;
    private int[] _triangles;
    private float _colorizing;
    
    private Mesh _outputMesh;
    
    private static readonly int Tiling = Shader.PropertyToID("_tiling");
    private static readonly int Offset = Shader.PropertyToID("_offset");
    private static readonly int Color = Shader.PropertyToID("_color");
    private static readonly int Blend = Shader.PropertyToID("_blend");

    #region Event Functions
    
    private void Start()
    {
        if (targetMeshFilter == null)
        {
            Debug.LogWarning("Mesh Filter is null! Returning...");
            return;
        }
        
        if (targetMeshRenderer == null)
        {
            Debug.LogWarning("Mesh Renderer is null! Returning...");
            return;
        }
        
        _outputMesh = new Mesh();
        targetMeshFilter.mesh = _outputMesh;
        
        GeneratePipe();
    }

    private void Update()
    {
        targetMeshRenderer.material.SetVector(Offset, new Vector2(Time.time * speed, 0));
        targetMeshRenderer.gameObject.transform.Rotate(new Vector3(0, 0, 1 * Time.deltaTime * rotationSpeed));
        targetMeshRenderer.material.SetColor(Color, gradient.Evaluate(_colorizing));

        _colorizing += Time.deltaTime * colorizingSpeed;

        if (_colorizing >= 1)
        {
            _colorizing = 0;
        }
    }

    private void OnValidate()
    {
        if(_outputMesh == null) return;
        
        targetMeshRenderer.material.SetVector(Blend, new Vector2(blend.x, blend.y));
        
        GeneratePipe();
    }

    #endregion
    
    #region Generation

    private void GeneratePipe()
    {
        var angle = 360f / edges;
        _vertices = new Vector3[(edges + 1) * (depth + 1)];
        _uvs = new Vector2[_vertices.Length];
        
        var rotation = 0f;
        
        for (int c = 0, i = 0; i <= depth; i++)
        {
            for (var j = 0; j <= edges; j++)
            {
                var pos = RotateAround(Vector3.right * radius, Quaternion.Euler(0, 0, j * angle), Vector3.zero);
                _vertices[c] = RotateAround(pos, Quaternion.Euler(rotation,0, 0), new Vector3(0, centerRadius, 0));
                c++;
            }

            rotation += maxAngle / depth;
        }
        
        for (int c = 0, i = 0; i <= depth; i++)
        {
            for (var j = 0; j <= edges; j++)
            {
                _uvs[c] = new Vector2((float)i / depth, (float)j / edges);
                c++;
            }
        }

        _triangles = new int[edges * depth * 6];
        var tris = 0;
        var vert = 0;
        
        for (var i = 0; i < depth; i++)
        {
            for (var j = 0; j < edges; j++)
            {
                _triangles[tris] = vert;
                _triangles[tris + 1] = vert + 1;
                _triangles[tris + 2] = vert + edges + 1;

                _triangles[tris + 3] = vert + 1;
                _triangles[tris + 4] = vert + edges + 2;
                _triangles[tris + 5] = vert + edges + 1;

                vert++;
                tris += 6;
            }

            vert++;
        }
        
        UpdateMesh();
    }

    private void UpdateMesh()
    {
        _outputMesh.Clear();
        
        _outputMesh.vertices = _vertices;
        _outputMesh.triangles = _triangles;
        _outputMesh.uv = _uvs;
        
        _outputMesh.RecalculateNormals();

        if (autoTiling)
        {
            tiling = new Vector2(depth, edges / autoTilingModifier);
        }
        
        targetMeshRenderer.material.SetVector(Tiling, tiling);
    }
    
    private Vector3 RotateAround(Vector3 target, Quaternion angle, Vector3 pivot)
    {
        return angle * (target - pivot) + pivot;
    }

    #endregion
}
